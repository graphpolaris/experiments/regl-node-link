import createRegl from 'regl';
import createLine from '../src/index.js';
import createScatterplot, { createRenderer }  from 'regl-scatterplot';
import result from './data.json';
import Stats from 'stats.js';

//------------------------------------------------------------------------------
// dots
//------------------------------------------------------------------------------

const canvas = document.getElementById("mycanvas");
const renderer = createRenderer();
let width = 500;
let height = 500;
// const { height, width } = canvas.getBoundingClientRect();

const scatterplot = createScatterplot({
  renderer,
  canvas,
  width,
  height,
  pointSize: 1
});

var dict = {};

//Gets all the datapoints from the result
function getDataPoints() {
  const points = [];
  result.value.nodes.forEach(node => {
    points.push(getDataPoint(node))
  });
  return points;
}

//Gets a datapoint from the result
function getDataPoint(point) {
  const x = -1 + parseFloat(point.x.replace(",", ".")) * 2;
  const y = -1 + parseFloat(point.y.replace(",", ".")) * 2;
  dict[point.id] = [x, y];
  return [x, y, 1]
}

scatterplot.set({pointColor: ['#FF0000']});

scatterplot.get('camera');

// getDataPoints();
scatterplot.draw(getDataPoints());

//------------------------------------------------------------------------------
// Init
//------------------------------------------------------------------------------

const numOfNodes = result.value.nodes.length;
const numOfEdges = result.value.edges.length;
const canvas2 = document.getElementById('mycanvas2');
const regl = createRegl(canvas2);

//------------------------------------------------------------------------------
// Point creation
//------------------------------------------------------------------------------

function getEdges() {
  const edges = []
  result.value.edges.forEach(edge => {
      edges.push(getEdge(edge));
  });
  return edges;
}

function getEdge(edge) {
  const [x, y] = dict[edge.from];
  const [x2, y2] = dict[edge.to];
  return [x, y, x2, y2]
}

function getRandomEdges(number) {
  const edges = []
  for (let i = 0; i < number; i++) {
    edges.push(getEdge());
  }
  return edges;
}

const edges = getEdges();
var sliced = 0;

function getEdgeBuffer(amountOfEdges){
  const buffer = edges.slice(sliced, sliced + amountOfEdges)
  sliced += amountOfEdges;
  return buffer; 
}


//------------------------------------------------------------------------------
// Line creation
//------------------------------------------------------------------------------

const lines = [];
const numOfBuffers = Math.ceil(numOfEdges / 10922)
for(var i = 0; i < numOfBuffers; i++){ 
  var amountOfEdges = 0;
  if((i + 1) * 10922 > numOfEdges) {
    amountOfEdges = numOfEdges % 10922
  } else {
    amountOfEdges = 10922;
  }
  lines.push(createLine(regl, {
    width: 1,
    color: [
      [0.75, 0.75, 0.75],
      [0.25, 0.25, 0.25]
    ],
    colorIndices: [0, 1, 0, 1],
    is2d: true,
    points: getEdgeBuffer(amountOfEdges)
  }));
}

//------------------------------------------------------------------------------
// Rendering
//------------------------------------------------------------------------------
regl.frame(() => {
  regl.clear({
    depth: 1,
  });
  const view = scatterplot.get('cameraView')

  lines.forEach(line => {
    line.draw({ view });
  });
});

const resizeCanvas = () => {
  // const { height, width } = canvas2.getBoundingClientRect();
  canvas2.height = height * window.devicePixelRatio;
  canvas2.width = width * window.devicePixelRatio;
};

resizeCanvas();

document.getElementById("nodes").innerHTML = "Num of nodes:" + numOfNodes;
document.getElementById("edges").innerHTML = "Num of edges:" + numOfEdges;

console.log(dict)


var stats = new Stats();
stats.showPanel( 1 ); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild( stats.dom );

function animate() {

	stats.begin();

	// monitored code goes here

	stats.end();

	requestAnimationFrame( animate );

}

requestAnimationFrame( animate );