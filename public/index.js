import createRegl from 'regl';
import createLine from '../src/lines.js';
import createScatterplot, { createRenderer } from 'regl-scatterplot';
import result from './Example-data/5milnodes-100kedges.json';
import Stats from 'stats.js';

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
const canvas = document.getElementById("mycanvas");
const renderer = createRenderer();
const { height, width } = canvas.getBoundingClientRect();
let aspectRatio = width / height
const pointColor = '#add8e6'
const lineColor = [1, 0.6, 0.5, 0.6]
const pointSize = 1
const lineWidth = 1



const scatterplot = createScatterplot({
  renderer,
  canvas,
  width,
  height,
  pointSize: pointSize
});

const numOfNodes = result.value.nodes.length;
const numOfEdges = result.value.edges.length;
const canvas2 = document.getElementById('mycanvas2');
const regl = createRegl(canvas2);

//------------------------------------------------------------------------------
// Nodes Creation
//------------------------------------------------------------------------------
var pointsDict = {};

//Gets all the datapoints from the result
function getDataPoints() {
  const points = [];
  result.value.nodes.forEach(node => {
    points.push(getDataPoint(node))
  });
  return points;
}

//Gets a datapoint from the result
function getDataPoint(point) {
  const x = -1 + parseFloat(point.x.replace(",", ".")) * 2;
  const y = -1 + parseFloat(point.y.replace(",", ".")) * 2;
  pointsDict[point.id] = [x, y];
  return [x, y, 1]
}

scatterplot.set({ pointColor: [pointColor] });
scatterplot.draw(getDataPoints());

//------------------------------------------------------------------------------
// Edges Creation
//------------------------------------------------------------------------------
function getEdges() {
  const edges = []
  result.value.edges.forEach(edge => {
    edges.push(getEdge(edge));
  });
  return edges;
}

function getEdge(edge) {
  const [x, y] = pointsDict[edge.from];
  const [x2, y2] = pointsDict[edge.to];
  return [x, y, x2, y2]
}

const edges = getEdges();

var sliced = 0;
function getEdgeBuffer(amountOfEdges) {
  const buffer = edges.slice(sliced, sliced + amountOfEdges)
  sliced += amountOfEdges;
  return buffer;
}

const lines = [];
const numOfBuffers = Math.ceil(numOfEdges / 10922)
for (var i = 0; i < numOfBuffers; i++) {
  var amountOfEdges = 0;
  if ((i + 1) * 10922 > numOfEdges) {
    amountOfEdges = numOfEdges % 10922
  } else {
    amountOfEdges = 10922;
  }
  lines.push(createLine(regl, {
    width: lineWidth,
    color: lineColor,
    is2d: true,
    points: getEdgeBuffer(amountOfEdges)
  }));
}

//------------------------------------------------------------------------------
// Rendering
//------------------------------------------------------------------------------
regl.frame(() => {
  regl.clear({
    depth: 1,
  });

  const tempView = scatterplot.get('cameraView');
  const view = [...tempView]
  view[12] = tempView[12] / aspectRatio
  lines.forEach(line => {
    line.draw({  view  });
  });
});

const resizeCanvas = () => {
  canvas2.height = height * window.devicePixelRatio;
  canvas2.width = width * window.devicePixelRatio;
};

scatterplot.set({
  aspectRatio: aspectRatio,
})

resizeCanvas();

//------------------------------------------------------------------------------
// Stats
//------------------------------------------------------------------------------
document.getElementById("nodes").innerHTML = "Num of nodes:" + numOfNodes;
document.getElementById("edges").innerHTML = "Num of edges:" + numOfEdges;

var stats = new Stats();
stats.showPanel(1); // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom);

function animate() {
  stats.begin();
  // monitored code goes here
  stats.end();
  requestAnimationFrame(animate);
}

requestAnimationFrame(animate);