# 2D Node-link visualisation

The #1 open-source library for running massive node-link visualisations!
This highly scalable node-link visualisation library uses WebGL through Regl. With this library, you are capable of rendering millions of nodes and links within seconds! This incredible amount is obtained by running the rendering tasks on the GPU using Regl and sacrificing feature richness. 

<p>
  <img src="./gifs/project.gif" />
</p>

**Default Interactions:**

- **Pan**: Click and drag your mouse.
- **Zoom**: Scroll vertically.
- **Rotate**: While pressing <kbd>ALT</kbd>, click and drag your mouse.
- **Select a dot**: Click on a dot with your mouse.
- **Select multiple dots**:
  - While pressing <kbd>SHIFT</kbd>, click and drag your mouse. All items within the lasso will be selected.
- **Deselect**: Double-click onto an empty region.

## Install

```
npm -i regl-line regl-scatterplot regl pub-sub-es regl-line
```

## Getting started

When cloning the code you will find the `public` folder, which contains the `index.html` and `index.js` files. `index.html` will be your main html file which contains two canvasses, one for the nodes and one for the links. So changing the size of your canvas means you have to change the size of both the canvasses.

in the `index.js` file in the `Initialization` section you will find a bunch of variables such as color and size which you can change to your liking.

### Examples

For examples data sets see the folder `public/Example-data`. 

## Specs

The node-link visualisation is tested with a maximum of 5 million nodes and 100 thousand edges. This compiles in about 20 seconds and renders in about a minute (depending on your hardware). After rendering it, the node-link visualisation runs smoothly with 30+ FPS while draging around, selecting and rotating. Only when zooming in and out the FPS can go down to 2 FPS. 

<p>
  <img src="./gifs/5mil-nodes.gif" />
</p>

The node-link visualisation can also handle more than 5 million nodes, however you will run into a `'ERR_STRING_TOO_LONG'` error. This is a limitation of Node.js.

## Data

For your json data the project expects a certain format:
```
{
  "value": {
    "nodes": [
      {
        "id": ...,
        "x": ...,
        "y": ...
      },
    ],
    "edges": [
      {
        "from": ...,
        "to": ...
      },
    ]
  }
}
```
**IMPORTANT:** Your points positions need to be normalized to `[-1, 1]` (normalized device coordinates).

## Credits
This node-link visualisation library is created by [Ruben Franquinet](https://www.linkedin.com/in/ruben-franquinet-93a852203/).
For the making of this node-link visualisation I have mainly used two libraries [regl-scatterplot](https://github.com/flekschas/regl-scatterplot) and [regl-line](https://github.com/flekschas/regl-line) both created by Fritz Lekschas. 

You can also use the documentation of these two projects to see even more advanced customisations for this project.